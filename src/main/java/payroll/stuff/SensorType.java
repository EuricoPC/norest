package payroll.stuff;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Data
@Table(name = "SENSORTYPE")
public class SensorType {

    public Long getId() {
        return id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(unique=true)
    private String typeName;

    public SensorType(){}

    /**
     * Constructor class SensorType     *
     *
     * @param typeName String with the designation of the sensor type
     */
    public SensorType(String typeName) {
        this.typeName = typeName;
    }


}




