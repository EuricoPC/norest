package payroll.stuff;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import payroll.sensorRest.Sensor;

import java.util.List;
import java.util.Optional;


@Service
public class SensorRepository {

    private SensorCrudRepository crudRepository;

    //empty constructor for Spring framework
    protected SensorRepository() {
    }

    @Autowired
    public SensorRepository(SensorCrudRepository crudRepo) {
        crudRepository = crudRepo;
    }

    /**
     * Creates and stores the new sensor in the DB.
     * If a sensor with the same ID already exists, the sensor will not be created (return false)
     */


    public List<Sensor>  findAll(){
        return crudRepository.findAll();
    }
    public Sensor save(Sensor newSensor){
        return crudRepository.save(newSensor);
    }

    public boolean sensorExists(String sensorID){
        return crudRepository.existsBySensorID(sensorID);
    }

    public Optional<Sensor> findBySensorID(String sensorID){
         return crudRepository.findBySensorID(sensorID);
    }

    public Sensor findById(Long id){
        return crudRepository.findSensorById(id);
    }

    public List<Sensor> getAll() {
        return crudRepository.findAll();
    }

    public List<Sensor> findSensorsByGeographicalArea(GeographicalArea geographicalArea){

        return crudRepository.findSensorsByGeographicalArea(geographicalArea);
    }

    public void deleteBySensorID(String sensorID){
        crudRepository.deleteBySensorID(sensorID);
    }
}
