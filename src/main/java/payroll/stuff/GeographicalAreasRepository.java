package payroll.stuff;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class GeographicalAreasRepository {

    private GeographicalAreaCrudRepository crudRepository;

    /**
     * Constructor
     */
    @Autowired
    public GeographicalAreasRepository(GeographicalAreaCrudRepository gaCrudRepository) {
        crudRepository = gaCrudRepository;
    }

    //empty constructor for spring framework
    protected GeographicalAreasRepository(){}

    /**
     * method to get the list of geographical areas
     *
     * @return list of geographical areas
     */
    public List<GeographicalArea> findGeographicAreas() {
        return crudRepository.findAll();
    }

    /**
     * method to get the geographical area in the given index
     */
    public GeographicalArea getGeographicalAreaWithIndex(int index) {
        return findGeographicAreas().get(index);
    }

    /**
     * method to get the geographical area by name
     */
    public Optional<GeographicalArea> getGeographicalAreaWithName(String name) {
        return crudRepository.getGeographicalAreaByName(name);
    }



    /**
     * Checks if there is any Geographical Area with this name stored in the DB
     */
    public boolean geographicalAreaAlreadyExists(String name) {
        int count = this.crudRepository.countGeographicalAreasByName(name);
        return (count != 0);
    }

    public boolean createGeographicalArea(GeographicalArea geographicalArea) {
        if (!geographicalAreaAlreadyExists(geographicalArea.getName())) {
            this.crudRepository.save(geographicalArea);
            return true;
        }
        return false;
    }

    public GeographicalArea save(GeographicalArea geographicalArea){
        return crudRepository.save(geographicalArea);
    }

    /**
     * method to add a geographical area to geographical area's list
     *
     * @param geographicalArea geographical area name
     * @return true when added to the list
     */
    public boolean add(GeographicalArea geographicalArea) {
        boolean result = false;

        if (!findGeographicAreas().contains(geographicalArea)) {
            result = findGeographicAreas().add(geographicalArea);
            crudRepository.save(geographicalArea);
        }
        return result;
    }



}