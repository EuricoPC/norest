package payroll.stuff;

public class SensorNotFoundException extends RuntimeException{

    public SensorNotFoundException(String sensorID) {
        super("Could not find Sensor " + sensorID);
    }
}
