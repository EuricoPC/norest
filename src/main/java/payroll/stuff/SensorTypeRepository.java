package payroll.stuff;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SensorTypeRepository {

    final private SensorTypeCrudRepository crudRepository;

    /**
     * Constructor
     */
    @Autowired
    public SensorTypeRepository(SensorTypeCrudRepository sensorTypeCrudRepository) {
        crudRepository = sensorTypeCrudRepository;
    }


    /**
     * Method to create and add a sensor type to the list of sensor types
     *
     * @param sensorType type of sensor
     * @return True if type of sensor is added to the list
     */
    public boolean addSensorTypes(SensorType sensorType) {
        if (!sensorTypeExists(sensorType.getTypeName())) {
            crudRepository.save(sensorType);
            return true;
        }
        return false;
    }


    /**
     * Method to create and add a sensor type to the list of sensor types
     *
     * @return True if type of sensor is added to the list
     */
    public boolean addSensorTypes(String string) {
        SensorType sensorType = new SensorType(string);
        return addSensorTypes(sensorType);
    }

    /**
     * Method to get the sensor type list
     *
     * @return Sensor type list
     */
    public List<SensorType> getTypeSensorList() {
        return crudRepository.findAll();
    }

    /**
     * Method to get the list of sensor types as string
     *
     * @return sensor type list as string
     */
    public List<String> getListAsString() {
        return getTypeSensorList().stream().map(SensorType::getTypeName).
                collect(Collectors.toList());
    }

    /**
     * Method to verify if sensorType exists in crudRepository of sensor types
     */
    public boolean sensorTypeExists(String typeName) {
        int count = crudRepository.countByTypeName(typeName);
        return count != 0;
    }


    public SensorType getSensorTypeDB(String string) {
        return crudRepository.findByTypeName(string);
    }


    /**
     * This method checks if there is any sensor type with the name sent as argument
     * If there's no type with that name, a new type is created. If there's a type with the same name,
     * then it will get it from the list of created types
     *
     * @return sensor type
     */
    public SensorType getOrCreateSensorType(String typeName) {

        boolean sTypeExists = sensorTypeExists(typeName);
        if (sTypeExists) {
            return getSensorTypeDB(typeName);
        }
        SensorType type = new SensorType(typeName);
        return crudRepository.save(type);
    }


}
