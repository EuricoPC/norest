package payroll.stuff;

import org.springframework.data.repository.CrudRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import payroll.sensorRest.Sensor;
import java.util.List;
import java.util.Optional;


@Repository
public interface SensorCrudRepository extends CrudRepository<Sensor, Long> {

    List<Sensor> findAll();

    Sensor findSensorById(Long id);

    boolean existsBySensorID(String sensorID);

    Optional<Sensor> findBySensorID(String sensorID);

    List<Sensor> findSensorsByGeographicalArea(GeographicalArea geographicalArea);

    @Transactional
    void deleteBySensorID(String sensorID);

}
