package payroll.stuff;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GeographicalAreaCrudRepository extends CrudRepository<GeographicalArea, Long> {

    List<GeographicalArea> findAll();

    int countGeographicalAreasByName(String name);

    Optional<GeographicalArea> getGeographicalAreaByName(String name);
}