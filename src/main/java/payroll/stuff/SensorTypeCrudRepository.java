package payroll.stuff;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SensorTypeCrudRepository extends CrudRepository<SensorType, Long> {

    List<SensorType> findAll();
    SensorType findByTypeName(String type);
    int countByTypeName(String sensorType);


}
