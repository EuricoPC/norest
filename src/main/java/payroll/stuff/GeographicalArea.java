package payroll.stuff;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "GEO_AREAS")

public class GeographicalArea {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    private String name;


    /**
     * Protected empty constructor for JPA usage
     */
    protected GeographicalArea() {
    }

    /**
     * Constructor of class geographical area
     *
     * @param name                   geographical area name
     */
    public GeographicalArea(String name) {
        this.name = name;
    }
}