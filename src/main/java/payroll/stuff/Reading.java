package payroll.stuff;


import lombok.Data;

import javax.persistence.Embeddable;
import java.util.Date;
import java.util.Objects;

@Embeddable
@Data
public class Reading {

    private Date dateTime;
    private double readingValue;



    protected Reading() {
    }

    /**
     * Constructor
     *
     * @param date         date of the reading
     * @param readingValue reading value
     */
    public Reading(Date date, double readingValue) {

        this.dateTime = (Date) date.clone();
        this.readingValue = readingValue;
    }


}
