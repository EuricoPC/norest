package payroll.stuff;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;


@Embeddable
@Data
public class Location {

    @Column(nullable = true)
    private double altitude;
    @Column(nullable = true)
    private double longitude;
    @Column(nullable = true)
    private double latitude;

    /**
     * Protected empty constructor for JPA usage
     */
    protected Location() {}


    /**
     * Location Constructor
     * @param altitude - altitude
     * @param longitude - longitude
     * @param latitude - latitude
     */
    public Location(double altitude, double longitude, double latitude) {
        this.altitude = altitude;
        this.longitude = longitude;
        this.latitude = latitude;
    }


}
