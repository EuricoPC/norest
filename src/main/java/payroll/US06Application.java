package payroll;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class US06Application {

    public static void main(String... args) {
        SpringApplication.run(US06Application.class, args);
    }
}