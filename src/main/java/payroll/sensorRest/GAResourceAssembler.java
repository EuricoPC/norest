package payroll.sensorRest;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;
import payroll.stuff.GeographicalArea;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class GAResourceAssembler implements ResourceAssembler<GeographicalArea, Resource<GeographicalArea>> {

        @Override
        public Resource<GeographicalArea> toResource(GeographicalArea ga) {

            return new Resource<>(ga,
                    linkTo(methodOn(GAController.class).one(ga.getName())).withSelfRel(),
                    linkTo(methodOn(GAController.class).all()).withRel("geo areas"));
        }
}
