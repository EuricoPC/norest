package payroll.sensorRest;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
class SensorListResourceAssembler implements ResourceAssembler<Sensor, Resource<Sensor>> {

    @Override
    public Resource<Sensor> toResource(Sensor sensor) {

        return new Resource<>(sensor,
                linkTo(methodOn(SensorController.class).one(sensor.getSensorID())).withSelfRel(),
                linkTo(methodOn(SensorController.class).all()).withRel("sensors"));
    }
}
