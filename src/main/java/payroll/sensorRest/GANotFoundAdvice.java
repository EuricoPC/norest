package payroll.sensorRest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GANotFoundAdvice {
    @ResponseBody
    @ExceptionHandler(GeoAreaNotFoundException.class) //configures the advice to only respond to this exception
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String GeoAreaNotFoundHandler(GeoAreaNotFoundException ex) {
        return ex.getMessage();
    }
}
