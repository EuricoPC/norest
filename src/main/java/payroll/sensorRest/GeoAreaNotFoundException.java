package payroll.sensorRest;

public class GeoAreaNotFoundException extends RuntimeException {

    public GeoAreaNotFoundException(String name) {
        super("Could not find Geographical Area " + name);
    }
}
