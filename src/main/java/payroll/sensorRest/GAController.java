package payroll.sensorRest;

import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import payroll.stuff.GeographicalArea;
import payroll.stuff.GeographicalAreasRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class GAController {

    private final GeographicalAreasRepository repository;
    private GAResourceAssembler assembler;

    public GAController(GeographicalAreasRepository gaRepository, GAResourceAssembler assembler) {
        this.repository = gaRepository;
        this.assembler = assembler;
    }

    @RequestMapping(value = "/geo-areas", method = RequestMethod.GET)
    ResponseEntity<Object> all() {
        List<GeographicalArea> geographicalAreas = repository.findGeographicAreas();
        List<Resource<GeographicalArea>> resources =
                geographicalAreas.stream().map(assembler::toResource)
                        .collect(Collectors.toList());
        return ResponseEntity.ok(resources);
    }


    @RequestMapping(value = "/geo-areas/{name}", method = RequestMethod.GET)
    ResponseEntity<Object> one(@PathVariable String name) {
        GeographicalArea geographicalArea = repository.getGeographicalAreaWithName(name)
                .orElseThrow(() -> new GeoAreaNotFoundException(name));
        Resource<GeographicalArea> resource = assembler.toResource(geographicalArea);
        return ResponseEntity.ok(resource);
    }
}
