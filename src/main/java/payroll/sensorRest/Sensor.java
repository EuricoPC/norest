package payroll.sensorRest;

import lombok.Data;
import payroll.stuff.*;

import javax.persistence.*;
import java.util.*;

@Entity
@Data
@Table(name = "SENSORS")
public class Sensor {

    private static final String ACTIVATE = "Active";
    private static final String DEACTIVATE = "Deactivate";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(unique = true)
    private String sensorID;
    private String name;
    @OneToOne
    private SensorType sensorType;

    @OneToOne
    private GeographicalArea geographicalArea;
    private Date startOperationDate;
    @Embedded
    private Location location;
    private boolean active;
    @ElementCollection(fetch = FetchType.EAGER)
    private Map<Date, String> logActivityStatus;
    @Enumerated(EnumType.STRING)
    private SensorLocationType sensorLocationType;
    @ElementCollection(fetch = FetchType.EAGER)
    private List<Reading> readings;


    /**
     * Protected empty constructor for JPA usage
     */
    protected Sensor() {
    }

     public Sensor(String sensorID, SensorType sensorType, Location location, Date startOperationDate, GeographicalArea geographicalArea) {
        this.readings = new ArrayList<>();
        this.sensorID = sensorID;
        this.sensorType = sensorType;
        this.startOperationDate = (Date) startOperationDate.clone();
        this.location = location;
        this.name = "";
        this.active = true;
        this.logActivityStatus = new HashMap<>();
        this.logActivityStatus.put(startOperationDate, ACTIVATE);
        this.sensorLocationType = SensorLocationType.OUTER;
        this.geographicalArea = geographicalArea;
        this.readings = new ArrayList<>();
    }
}
