package payroll.sensorRest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import payroll.stuff.SensorType;
import payroll.stuff.SensorTypeRepository;

import java.util.List;

@RestController
public class STController {

    private final SensorTypeRepository typeRepository;

    public STController (SensorTypeRepository typeRepository){
        this.typeRepository = typeRepository;
    }

    @RequestMapping(value = "/sensor-types", method = RequestMethod.GET)
    ResponseEntity<Object> all() {
        List<SensorType> types = typeRepository.getTypeSensorList();
        return new ResponseEntity<>(types, HttpStatus.OK);
    }

}
