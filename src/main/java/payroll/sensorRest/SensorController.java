package payroll.sensorRest;

import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import payroll.stuff.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class SensorController {

    private final SensorRepository repository;

    private final SensorResourceAssembler sensorResourceAssembler;

    private final SensorListResourceAssembler sensorListResourceAssembler;

    public SensorController(SensorRepository repository, SensorResourceAssembler assembler, SensorListResourceAssembler sensorListResourceAssembler) {
        this.repository = repository;
        this.sensorResourceAssembler = assembler;
        this.sensorListResourceAssembler = sensorListResourceAssembler;
    }


    @RequestMapping(value = "/sensors", method = RequestMethod.POST)
    ResponseEntity<Object> newSensor(@RequestBody Sensor newSensor) {
        String newID = newSensor.getSensorID();
        if (repository.sensorExists(newID)) {
            return new ResponseEntity<>("It was impossible to create this sensor, sensor already exists.", HttpStatus.UNPROCESSABLE_ENTITY);
        }
        repository.save(newSensor);
        return new ResponseEntity<>("New sensor successfully added to the geographical area.", HttpStatus.OK);
    }

    @RequestMapping(value = "/sensors", method = RequestMethod.GET)
    ResponseEntity<Object> all() {
        List<Sensor> sensors = repository.getAll();
        List<Resource<Sensor>> resources = sensors
                .stream().map(sensorListResourceAssembler::toResource)
                .collect(Collectors.toList());
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @RequestMapping(value = "/sensors/{sensorID}", method = RequestMethod.GET)
    ResponseEntity<Object> one(@PathVariable String sensorID) {
        Sensor sensor = repository.findBySensorID(sensorID)
                .orElseThrow(() -> new SensorNotFoundException(sensorID));
        Resource<Sensor> resource = sensorResourceAssembler.toResource(sensor);
        return ResponseEntity.ok(resource);
    }

//    @RequestMapping(value = "/sensors/{sensorID}", method = RequestMethod.DELETE)
//    ResponseEntity<Object> delete(@PathVariable String sensorID) {
//        repository.deleteBySensorID(sensorID);
//        return ResponseEntity.noContent().build();
//    }

    @RequestMapping(value = "/sensors/{sensorID}", method = RequestMethod.DELETE)
    ResponseEntity<Object> deleteSensor(@PathVariable String sensorID) {

        repository.deleteBySensorID(sensorID);

        return new ResponseEntity<>(sensorID + " has been deleted", HttpStatus.NO_CONTENT);
    }
}
