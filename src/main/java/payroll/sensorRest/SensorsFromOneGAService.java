package payroll.sensorRest;

import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import payroll.stuff.GeographicalArea;
import payroll.stuff.GeographicalAreasRepository;
import payroll.stuff.SensorRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class SensorsFromOneGAService {

    private final SensorRepository sensorRepository;

    private final GeographicalAreasRepository geographicalAreasRepository;

    private final SensorResourceAssembler assembler;

    public SensorsFromOneGAService(GeographicalAreasRepository geographicalAreasRepository, SensorRepository sensorRepository, SensorResourceAssembler assembler) {
        this.sensorRepository = sensorRepository;
        this.assembler = assembler;
        this.geographicalAreasRepository = geographicalAreasRepository;
    }

    @RequestMapping(value = "/geo-areas/{name}/sensors", method = RequestMethod.GET)
    ResponseEntity<Object> allSensorsFromGA(@PathVariable String name) {

        GeographicalArea geographicalArea = geographicalAreasRepository.getGeographicalAreaWithName(name)
                .orElseThrow(() -> new GeoAreaNotFoundException(name));

        List<Sensor> sensorList = sensorRepository.findSensorsByGeographicalArea(geographicalArea);

        List<Resource<Sensor>> resources = sensorList.stream().map(assembler::toResource)
                .collect(Collectors.toList());
        return new ResponseEntity<>(resources, HttpStatus.OK);
    }
}
