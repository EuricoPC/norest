package payroll;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import payroll.sensorRest.Sensor;
import payroll.stuff.*;

import java.util.Date;

@Configuration
@Slf4j
class LoadDatabase {

    @Bean
    CommandLineRunner initDatabase(SensorRepository senRepo, GeographicalAreasRepository gaRepo, SensorTypeRepository tyRepo) {
        return args -> {
            SensorType type1 = tyRepo.getOrCreateSensorType("temp");
            SensorType type2 = tyRepo.getOrCreateSensorType("rain");
            log.info("Preloading " + type1);
            log.info("Preloading " + type2);
            GeographicalArea ga1 = gaRepo.save(new GeographicalArea("ISEP"));
            GeographicalArea ga2 = gaRepo.save(new GeographicalArea("Porto"));
            log.info("Preloading " + ga1);
            log.info("Preloading " + ga2);
            Location location1 = new Location(1,2, 3);
            Location location2 = new Location(3,2,1);
            Date date1 = new Date();
            log.info("Creating Sensor 1" + senRepo.save(
                    new Sensor("sensor1",type1, location1, date1, ga1)));
            log.info("Creating Sensor 2" + senRepo.save(
                    new Sensor("sensor2",type2, location2, date1, ga1)));

        };
    }
}