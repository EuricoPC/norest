package old;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {

    private static final String EMPLOYEE_PATH = "/employees";
    private static final String FIRST_EMPLOYEE_PATH = "/employees/1";
    private static final String SECOND_EMPLOYEE_PATH = "/employees/2";
    private static final String THIRD_EMPLOYEE_PATH = "/employees/5";

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenReadAll_thenStatusIsOk() throws Exception {
        this.mockMvc.perform(get(EMPLOYEE_PATH)).andExpect(status().isOk());
    }

    @Test
    public void whenNewEmployee_thenStatusIsOK() throws Exception {
        Employee employee = new Employee("Bruno", "Moreira", "Slave");
        this.mockMvc.perform(post(EMPLOYEE_PATH)
                .content(asJsonString(employee))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());

        this.mockMvc.perform(get(THIRD_EMPLOYEE_PATH)).andExpect(status().isOk()); //why not status().isCreated()
    }

    @Test
    public void acceptJsonMediaType() throws Exception {
        Employee employee = new Employee("Bruno", "Moreira", "Slave");
        mockMvc.perform(post(EMPLOYEE_PATH)
                .content(asJsonString(employee))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));
    }

    @Test
    public void whenGetDeletedEmployee_thenStatusIsNotFound() throws Exception {
        this.mockMvc.perform(delete(FIRST_EMPLOYEE_PATH)).andExpect(status().isNoContent());
        this.mockMvc.perform(get(FIRST_EMPLOYEE_PATH)).andExpect(status().isNotFound());
    }

    @Test
    public void whenUpdate_thenStatusIsOK() throws Exception {
        Employee newEmployeeInfo = new Employee("Eurico", "Costa", "Master");
        this.mockMvc.perform(put(SECOND_EMPLOYEE_PATH)
                .content(asJsonString(newEmployeeInfo))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
